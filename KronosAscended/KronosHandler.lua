--Variables
local players = game.Players:GetPlayers()
local AdminPanel = script.Assets["GUI's"].AdminPanel
local cmds = require(script.Commands)
local Settings
local Roles
local cmd
local playerPerm
--Main
local Main = {}
--AddGui
function Main:Initalize(RequiredConfig)
	--Setting config vars
	Settings = RequiredConfig
	Roles = Settings.BanStore
	
	--Other
	local con = game.Players.PlayerAdded:Connect(function(player)
		local temp = require(script.Commands.BanSystem)
		if temp.CheckBan(Settings.BanStore, player) then
			player:Kick("You are banned from this game.")
		else
			Main.AttachChatListener(player)
		end
	end)
end
Main.AttachChatListener = function(player)
	player.Chatted:Connect(function(msg)
		for i,v in pairs(Roles) do
			if table.find(v[2], player.Name) then
				playerPerm = v[1]
			end
		end
		if playerPerm == nil then
			playerPerm = "Non-Admin"
		end
		if string.sub(msg, 1,1) == Settings.Prefix and msg ~= Settings.Prefix then
			for i, commandTable in pairs(cmds) do
				if type(commandTable) == "table" and commandTable.Name ~= nil then
					local cmdPermTable = commandTable.Permissions
					if type(cmdPermTable) == "table" then
					end
					local commandTableName = string.lower(commandTable.Name)
					local cmdPrefixSplit = string.split(msg, Settings.Prefix)
					local params = string.split(cmdPrefixSplit[2], " ")
					local cmd = string.lower(params[1])
					table.remove(params, 1)
					if table.find(commandTable.Permissions, playerPerm) then
						if string.match(cmd, commandTableName) or table.find(commandTable.Aliases, cmd) then
							commandTable.Start(player, params, Settings)
						end
					end
				end
			end
		end
	end)
end
Main.AddGui = function(player)
	local newGui = AdminPanel:Clone()
	newGui.Parent = game.Players[player.Name].PlayerGui
	print(player, " had their AdminGui Added")
end

return Main
