local Plrs = game.Players:GetChildren()
local module = {}
local Config = require(script.Parent.Config)
local TargetFinder = require(script["TargetFinder(CORE)"])

local module = {
--- WalkSpeed ---
{

Name = "walkspeed",
Aliases = {"ws", "speed"},
Permissions = {"Owner","Admin","Moderator"},
Start = function(playerWhoUsedCommand, params)
local CMDRunner = function(targets)
	pcall(function()
		for i,v in pairs(targets) do
		game.Workspace[tostring(v)].Humanoid.WalkSpeed = params[2]
		print(v)
		end
	end)
end;
local targets = TargetFinder.FindTarget(playerWhoUsedCommand, params[1])
CMDRunner(targets)
end;

};
--- End WalkSpeed ---
--- Kill ---
{

Name = "kill",
Aliases = {},
Permissions = {"Owner","Admin","Moderator"},
Start = function(playerWhoUsedCommand, params)
local CMDRunner = function(targets)
	pcall(function()
		for i,v in pairs(targets) do
		game.Workspace[tostring(v)].Humanoid.Health = 0
		print(v)
		end
	end)
end;
local targets = TargetFinder.FindTarget(playerWhoUsedCommand, params[1])
CMDRunner(targets)
end;

};
--- End Kill ---
--- WalkSpeed ---
{

Name = "jumppower",
Aliases = {"jp"},
Permissions = {"Owner","Admin","Moderator"},
Start = function(playerWhoUsedCommand, params)
local CMDRunner = function(targets)
	pcall(function()
		for i,v in pairs(targets) do
		game.Workspace[tostring(v)].Humanoid.JumpPower = params[2]
		print(v)
		end
	end)
end;
local targets = TargetFinder.FindTarget(playerWhoUsedCommand, params[1])
CMDRunner(targets)
end;

};
--- End WalkSpeed ---




-----------SPECIAL COMMANDS (DO NOT EDIT BELOW THIS LINE)-----------
--- ModCall ---
{

Name = "modcall",
Aliases = {},
Permissions = {"Owner","Admin","Moderator","Non-Admin"},
Start = function(playerWhoUsedCommand, params)
local CMDRunner = function()
		local modcall = require(script.ModCallSystem)
		modcall.Call(playerWhoUsedCommand, params)
end;
CMDRunner()
end;

};
--- End Modcall ---
--- Recieve ---
{

Name = "recieve",
Aliases = {},
Permissions = {"Owner","Admin","Moderator","Non-Admin"},
Start = function(playerWhoUsedCommand, params)
local CMDRunner = function()
pcall(function()
	local modcall = require(script.ModCallSystem)
	modcall.Recieve(playerWhoUsedCommand, params[1])
end)
end;
CMDRunner()
end;

};
--- End Recieve ---
}
return module
