local module = {}
module.Call = function(playerCalling, params)
	local CallStoreHighestStore = game:GetService("DataStoreService"):GetDataStore("ModCallStoreHighest")
	local highest = CallStoreHighestStore:GetAsync("HighestCase")
	if highest then
	CallStoreHighestStore:SetAsync("HighestCase", highest + 1)
	else
		CallStoreHighestStore:SetAsync("HighestCase", 1)
	end
	
	local CallStore = game:GetService("DataStoreService"):GetDataStore("ModCallStore")
	
	CallStore:SetAsync(CallStoreHighestStore:GetAsync("HighestCase"), game.JobId)
	print("Sending")
	local str = "Incoming ModCall: "
	for i,v in pairs(params) do
		str = str .. " " .. v
	end
	local toSend = str
	local httpService = game:GetService("HttpService")
	local data = {
		["content"] = toSend .. " (" .. playerCalling.Name .. ") " .. "CaseID is: " .. CallStoreHighestStore:GetAsync("HighestCase")
	
	}
	data = httpService:JSONEncode(data)
	httpService:PostAsync("", data)
	print("sent")
end
module.Recieve = function(player, CaseID)
	local CallStore = game:GetService("DataStoreService"):GetDataStore("ModCallStore")
	game:GetService("TeleportService"):TeleportToPlaceInstance(game.PlaceId, CallStore:GetAsync(CaseID), player)
end
return module
