local statHandler = require(script.Parent.StatHandler)
local module = {}
module.Ban = function(store, player)
	statHandler.SetPlayerStat(store, player, true)
	return "Banned"
end
module.RemoveBan = function(store, player)
	statHandler.SetPlayerStat(store, player, true)
	return "Unbanned"
end
module.CheckBan = function(store, player)
	local banned = statHandler.GetStatFromPlayer(store, player)
	if banned then
		return true
	end
end
return module
