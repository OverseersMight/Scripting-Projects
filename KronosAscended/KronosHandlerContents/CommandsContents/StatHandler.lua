local module = {}
module.AddStatToPlayer = function(datastore, player, amount)
	local store = game:GetService("DataStoreService"):GetDataStore(tostring(datastore) .. "-KronosStore")
	local data = store:GetAsync("data")
	local found = false
	print(#data)
	if data == nil or type(data) ~= "table" or #data == 0 then
		warn("KronosAscended, Error Code: 1")
		wait(1)
		warn("Reason: Datastore Malformed")
		wait(1)
		warn("Automated Procedure: Resetting data and inputting PlayerData")
		data = {}
		table.insert(data, {})
		data[#data][1] = player.UserId
		data[#data][2] = amount
		store:SetAsync("data", data)
	else
		for i,v in pairs(data) do
			if #v == 2 then
				if v[1] == player.UserId then
					found = true
					if type(v[2]) == "number" then
						v[2] = v[2] + amount
						store:SetAsync("data", data)
					else
						warn("KronosAscended, Error Code: 2")
						wait(1)
						warn("Reason: PlayerData is not numeric")
						wait(1)
						warn("Automated Procedure: N/A")
					end
				end	
			end
		end
		if found == false then
			warn("KronosAscended, Error Code: 3")
			wait(1)
			warn("Reason: PlayerData not found")
			wait(1)
			warn("Automated Procedure: Adding new DataLine and inputting PlayerData")
			table.insert(data, {})
			table.insert(data, #data + 1,player.UserId)
			table.insert(data, #data + 1, amount)
			store:SetAsync("data", data)
		end
	end
end
module.SetPlayerStat = function(datastore, player, amount)
	local store = game:GetService("DataStoreService"):GetDataStore(tostring(datastore) .. "-KronosStore")
	local data = store:GetAsync("data")
	local found = false
	print(#data)
	if data == nil or type(data) ~= "table" or #data == 0 then
		warn("KronosAscended, Error Code: 1")
		wait(1)
		warn("Reason: Datastore Malformed")
		wait(1)
		warn("Automated Procedure: Resetting Data and Inputting User Data")
		data = {}
		table.insert(data, {})
		data[#data][1] = player.UserId
		data[#data][2] = amount
	else
		for i,v in pairs(data) do
			if #v == 2 then
				if v[1] == player.UserId then
					found = true
					v[2] = amount
					store:SetAsync("data", data)
				end
			end
		end
		if found == false then
			warn("KronosAscended, Error Code: 3")
			wait(1)
			warn("Reason: PlayerData not found when inputting new PlayerData")
			wait(1)
			warn("Automated Procedure: Adding new DataLine and inputting PlayerData")
			table.insert(data, {})
			table.insert(data, #data + 1,player.UserId)
			table.insert(data, #data + 1, amount)
			store:SetAsync("data", data)
		end
	end
end
module.GetStatFromPlayer = function(datastore, player)
		local store = game:GetService("DataStoreService"):GetDataStore(tostring(datastore) .. "-KronosStore")
		local data = store:GetAsync("data")
		local found
		if type(data)=="table" then
		for i,v in pairs(data) do
			if type(v) == "table" then
				if #v == 2  then
					if v[1] == player.UserId then
						return v[2]
					end
				end		
			end
		end
		warn("KronosAscended, Error Code: 4")
		wait(1)
		warn("Reason: PlayerData not found when retrieving PlayerData")
		wait(1)
		warn("Automated Procedure: N/A")
	end
	warn("KronosAscended, Error Code: 5")
	wait(1)
	warn("Reason: No Data")
	wait(1)
	warn("Automated Procedure: N/A")
end
module.GetStoreData = function(datastore)
	local store = game:GetService("DataStoreService"):GetDataStore(tostring(datastore) .. "-KronosStore")
	local data = store:GetAsync("data")
	return data
end
return module
