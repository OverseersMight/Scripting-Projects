local module = {}
module.FindTarget = function(playerWhoUsedCommand, param1)
	-- Use On All Players --
	if param1 == "all" then
		local targets = {}
		for i, target in pairs(game.Players:GetPlayers()) do
			table.insert(targets, target)
		end
		print("Used On All Players")
		return targets
	-- End --

	-- Use On Others --
	elseif param1 == "others" then
		local targets = {}
		for i, target in pairs(game.Players:Players()) do
			if target.Name ~= playerWhoUsedCommand.Name then
				table.insert(targets, target)
			end
		end
		print("Used on others")
		return targets
	-- End --

	-- Self Use --
	elseif param1 == "me" then
		print("Self Use")
		return {playerWhoUsedCommand}
	-- End --

	-- Self Use --
	elseif param1 == nil then
		print("Self Use")
		return {playerWhoUsedCommand}
	-- End --
	end
end
return module
